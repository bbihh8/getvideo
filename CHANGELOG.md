# ======== <br> <code>Change Log</code> <br> ========

`1.3.0`_ (2019-07-18)
---------------------
* Ajax request
* Updated Version Control

`1.2.0`_ (2018-11-05)
---------------------
* Version Control

`1.1.8`_ (2018-11-05)
---------------------
* Bug fix with preloader...

`1.1.5`_ (2018-11-05)
---------------------

* Scan URL for all video TagName's
* Added preloader & Tell user when they can scan page for video TagName



`1.0.0`_ (2018-11-03)
---------------------

* First Commit
* Deploy
* Scan URL for specific webpage...
